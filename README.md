# RunPythonInExe

Creates a single userfriendly .exe file which simply runs a .py script (Or anything else) over a systemCall.


  - Can have a FileParameter (Useful for "Open with...")
  - Gets the Path from the exe and executes the .py script in the right directory (Useful for the Fileparameter)
  - With an Icon ;)


### Requirements

* [MinGW](https://sourceforge.net/projects/mingw/?source=top3_dlp_t5) - MinGW - Minimalist GNU for Windows


### Usage
  - Edit the startExe.cpp to match your path and .py Script.
  - Replace the icon or delete it
  - Simply run the "compile.bat".



